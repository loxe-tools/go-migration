package loxe

import "fmt"

type transactionStyle string

const (
	TxPerOperation transactionStyle = "perMigration"
	UniqueTx                        = "uniqueTx"
)

func (t transactionStyle) isValid() bool {
	switch t {
	case TxPerOperation, UniqueTx:
		return true
	}
	return false
}

type Config struct {
	TxStyle                transactionStyle
	Schema                 string
	Tablespace             string
	SchemaVersionTableName string
}

func validateConfig(c Config) error {
	if !c.TxStyle.isValid() {
		return ErrInvalidTxStyle
	}
	if c.Schema == "" {
		return ErrInvalidSchema
	}
	if c.Tablespace == "" {
		return ErrInvalidTablespace
	}
	if c.SchemaVersionTableName == "" {
		return ErrInvalidSchemaVersionTableName
	}
	return nil
}

var DefaultConfig = Config{
	TxStyle:                UniqueTx,
	Schema:                 "public",
	Tablespace:             "pg_default",
	SchemaVersionTableName: "schema_version",
}

var ErrInvalidTxStyle = fmt.Errorf("[config] the given transaction style is invalid")
var ErrInvalidSchema = fmt.Errorf("[config] the schema name string cannot be empty")
var ErrInvalidTablespace = fmt.Errorf("[config] the tablespace cannot be empty")
var ErrInvalidSchemaVersionTableName = fmt.Errorf("[config] the schema version table name string cannot be empty")
