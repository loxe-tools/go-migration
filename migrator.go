package loxe

import (
	"database/sql"
	"fmt"
	"sort"
)

type Migrator interface {
	Up() error
	UpTo(uint64) error
	Down() error
	DownTo(uint64) error
}

type migrator struct {
	db             *sql.DB
	config         Config
	initialVersion uint64
	targetVersion  uint64
	finalVersion   uint64
	migrations     []Migration
}

func NewMigrator(db *sql.DB, config Config, migrations []Migration) (Migrator, error) {
	if db == nil {
		return nil, ErrNilSqlDB
	}
	if migrations == nil {
		return nil, ErrNilMigrations
	}
	configValidationErr := validateConfig(config)
	if configValidationErr != nil {
		return nil, configValidationErr
	}
	migrationsErr := normalizeMigrations(migrations)
	if migrationsErr != nil {
		return nil, migrationsErr
	}

	return &migrator{
		db:             db,
		config:         config,
		initialVersion: 0,
		targetVersion:  0,
		finalVersion:   0,
		migrations:     migrations,
	}, nil
}

func normalizeMigrations(migrations []Migration) error {
	sort.Slice(migrations, func(i, j int) bool { return migrations[i].Version() < migrations[j].Version() })
	for i := 1; i < len(migrations); i++ {
		if migrations[i-1].Version() != uint64(i) {
			return ErrNonSequentialMigrations
		}
	}

	return nil
}

var ErrNilSqlDB = fmt.Errorf("[constructor] the *sql.DB cannot be nil")
var ErrNilMigrations = fmt.Errorf("[constructor] the []Migration cannot be nil")
var ErrNonSequentialMigrations = fmt.Errorf("[migrations] the versions should initialize at 1 (one) and sequentially grow")
