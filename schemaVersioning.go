package loxe

import (
	"database/sql"
	"fmt"
)

func (m *migrator) prepareDB(tx *sql.Tx) error {
	hasSchemaVersionTable, e := m.hasSchemaVersionTable()
	if e != nil {
		return e
	}
	if hasSchemaVersionTable {
		return nil
	}

	if tx == nil {
		e := m.transaction(m.createSchemaVersionTable)
		if e != nil {
			return e
		}

		return nil
	}

	e = m.createSchemaVersionTable(tx)
	if e != nil {
		return e
	}

	return nil
}

func (m *migrator) hasSchemaVersionTable() (bool, error) {
	var exists bool
	checkSQL := fmt.Sprintf(checkIfTableExistsSQL, m.config.Schema, m.config.SchemaVersionTableName)
	row := m.db.QueryRow(checkSQL)
	e := row.Scan(&exists)
	if e != nil {
		return false, e
	}

	return exists, nil
}

func (m *migrator) createSchemaVersionTable(tx *sql.Tx) error {
	createSql := fmt.Sprintf(createSchemaVersionTableSQL, m.config.Schema, m.config.SchemaVersionTableName, m.config.Tablespace)
	_, e := tx.Exec(createSql)
	if e != nil {
		return e
	}

	versionRowSql := fmt.Sprintf(insertFirstSchemaVersionSQL, m.config.Schema, m.config.SchemaVersionTableName)
	_, e = tx.Exec(versionRowSql)
	if e != nil {
		return e
	}

	return nil
}

func (m *migrator) schemaVersion(tx *sql.Tx) (uint64, error) {
	var version uint64
	selectSQL := fmt.Sprintf(selectSchemaVersionSQL, m.config.Schema, m.config.SchemaVersionTableName)
	var row *sql.Row
	if tx != nil {
		row = tx.QueryRow(selectSQL)
	} else {
		row = m.db.QueryRow(selectSQL)
	}
	e := row.Scan(&version)
	if e != nil {
		return 0, e
	}

	return version, nil
}

const updateSchemaVersionSQL = `UPDATE %s.%s SET version = %d;`
const selectSchemaVersionSQL = `SELECT version FROM %s.%s;`
const insertFirstSchemaVersionSQL = `INSERT INTO %s.%s (version) VALUES (0);`
const createSchemaVersionTableSQL = `CREATE TABLE %s.%s (version BIGINT NOT NULL) TABLESPACE %s;`
const checkIfTableExistsSQL = `SELECT EXISTS (
   SELECT FROM pg_catalog.pg_class c
   JOIN   pg_catalog.pg_namespace n ON n.oid = c.relnamespace
   WHERE  n.nspname = '%s'
   AND    c.relname = '%s'
   AND    c.relkind = 'r'    -- only tables
);`
