package main

import (
	"database/sql"
	"flag"
	"fmt"
	_ "github.com/lib/pq"
	loxe "gitlab.com/loxe-tools/go-migration"
	"math"
)

type args struct {
	databaseHost     string
	databasePort     uint
	databaseName     string
	user             string
	pass             string
	migrationsFolder string
	down             bool
	targetVersion    uint64
}

func getArgs() args {
	dbHostPtr := flag.String("host", "localhost", "Database host address")
	dbPortPtr := flag.Uint("port", 5432, "Database host port")
	dbNamePtr := flag.String("dbName", "loxe", "Database name")
	userPtr := flag.String("user", "loxe", "Database username")
	passPtr := flag.String("pass", "123", "Database password")
	migrationsFolderPtr := flag.String("migrations", "./", "Path to the folder containing the migration files")
	downPtr := flag.Bool("down", false, "If true, the database will be DOWNgraded, otherwise UPgraded")
	targetVersionPtr := flag.Uint64("version", 0, "The target version to migrate the database. If not provided, will be math.MaxUint64 for UP and zero for DOWN")

	flag.Parse()
	if *targetVersionPtr == 0 && !*downPtr {
		*targetVersionPtr = math.MaxUint64
	}

	return args{
		databaseHost:     *dbHostPtr,
		databasePort:     *dbPortPtr,
		databaseName:     *dbNamePtr,
		user:             *userPtr,
		pass:             *passPtr,
		migrationsFolder: *migrationsFolderPtr,
		down:             *downPtr,
		targetVersion:    *targetVersionPtr,
	}
}

func connect(host string, port uint, username, database, password string) (*sql.DB, error) {
	sourceStr := fmt.Sprintf(
		"host=%s port=%d user=%s dbname=%s password=%s sslmode=%s",
		host, port, username, database, password, "disable",
	)
	return sql.Open("postgres", sourceStr)
}

func main() {
	args := getArgs()

	connection, e := connect(args.databaseHost, args.databasePort, args.user, args.databaseName, args.pass)
	if e != nil {
		panic(e)
	}

	migrations, e := loxe.MigrationsFromDir(args.migrationsFolder)
	if e != nil {
		panic(e)
	}

	migrator, e := loxe.NewMigrator(connection, loxe.DefaultConfig, migrations)
	if e != nil {
		panic(e)
	}

	if args.down {
		e := migrator.DownTo(args.targetVersion)
		if e != nil {
			panic(e)
		}
	} else {
		e := migrator.UpTo(args.targetVersion)
		if e != nil {
			panic(e)
		}
	}

	fmt.Println("...Finished")
}
