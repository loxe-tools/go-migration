package loxe

type Migration interface {
	Version() uint64
	Up() string
	Down() string
}

type migration struct {
	version uint64
	up      string
	down    string
}

func (m *migration) Version() uint64 { return m.version }
func (m *migration) Up() string      { return m.up }
func (m *migration) Down() string    { return m.down }
