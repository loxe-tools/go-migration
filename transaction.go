package loxe

import "database/sql"

type transactionFN func(tx *sql.Tx) error

func (m *migrator) transaction(fn transactionFN) error {
	tx, e := m.db.Begin()
	if e != nil {
		return e
	}
	defer tx.Rollback()

	e = fn(tx)
	if e != nil {
		return e
	}

	e = tx.Commit()
	if e != nil {
		return e
	}

	return nil
}
