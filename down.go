package loxe

import (
	"database/sql"
	"fmt"
)

func (m *migrator) DownTo(toVersion uint64) error {
	m.targetVersion = toVersion
	isDesiredVersion := func(actualVersion, migrationVersion uint64) bool {
		return actualVersion >= migrationVersion && migrationVersion > toVersion
	}
	return m.down(isDesiredVersion)
}

func (m *migrator) Down() error {
	m.targetVersion = 0
	isDesiredVersion := func(actualVersion, migrationVersion uint64) bool {
		return actualVersion >= migrationVersion
	}
	return m.down(isDesiredVersion)
}

func (m *migrator) down(isDesiredVersion func(actualVersion, migrationVersion uint64) bool) error {
	var e error

	switch m.config.TxStyle {
	case TxPerOperation:
		e = m.downTxPerMigration(isDesiredVersion)
	case UniqueTx:
		e = m.downUniqueTx(isDesiredVersion)
	}

	if m.targetVersion >= m.initialVersion {
		fmt.Printf("The target version (v%d) is greater/equal than the actual version (v%d)\n", m.targetVersion, m.initialVersion)
		return e
	}
	if m.finalVersion < m.initialVersion {
		fmt.Printf("Database downgraded from v%d to v%d\n", m.initialVersion, m.finalVersion)
		return e
	}

	fmt.Printf("There are no migrations lower than the actual version (v%d)\n", m.initialVersion)
	return e
}

func (m *migrator) downUniqueTx(isDesiredVersion func(actualVersion, migrationVersion uint64) bool) error {
	return m.transaction(func(tx *sql.Tx) error {
		e := m.prepareDB(tx)
		if e != nil {
			return e
		}

		m.initialVersion, e = m.schemaVersion(tx)
		if e != nil {
			return e
		}

		if m.targetVersion >= m.initialVersion {
			return nil
		}

		m.finalVersion = m.initialVersion
		for i := len(m.migrations) - 1; i >= 0; i-- {
			migration := m.migrations[i]
			migrationVersion := migration.Version()
			if !isDesiredVersion(m.initialVersion, migrationVersion) {
				continue
			}

			_, e := tx.Exec(migration.Down())
			if e != nil {
				return e
			}
			m.finalVersion = migrationVersion - 1
		}

		if m.finalVersion < m.initialVersion {
			_, e = tx.Exec(fmt.Sprintf(updateSchemaVersionSQL, m.config.Schema, m.config.SchemaVersionTableName, m.finalVersion))
			if e != nil {
				return e
			}
		}

		return nil
	})
}

func (m *migrator) downTxPerMigration(isDesiredVersion func(actualVersion, migrationVersion uint64) bool) error {
	e := m.prepareDB(nil)
	if e != nil {
		return e
	}

	m.initialVersion, e = m.schemaVersion(nil)
	if e != nil {
		return e
	}

	if m.targetVersion >= m.initialVersion {
		return nil
	}

	m.finalVersion = m.initialVersion
	for i := len(m.migrations) - 1; i >= 0; i-- {
		migration := m.migrations[i]
		migrationVersion := migration.Version()
		if !isDesiredVersion(m.initialVersion, migrationVersion) {
			continue
		}

		e = m.transaction(func(tx *sql.Tx) error {
			_, e := tx.Exec(migration.Down())
			if e != nil {
				return e
			}

			_, e = tx.Exec(fmt.Sprintf(updateSchemaVersionSQL, m.config.Schema, m.config.SchemaVersionTableName, migrationVersion - 1))
			if e != nil {
				return e
			}

			return nil
		})
		if e != nil {
			return e
		}
		m.finalVersion = migrationVersion - 1
	}

	return nil
}
