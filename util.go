package loxe

import (
	"io/ioutil"
	"os"
	"path/filepath"
	"strconv"
	"strings"
)

func MigrationsFromDir(dir string) ([]Migration, error) {
	migrations := []Migration{}
	e := filepath.Walk(dir, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if info.IsDir() {
			return nil
		}
		if !strings.HasSuffix(info.Name(),  ".down.sql") {
			return nil
		}

		migration, e := createMigration(path, info)
		if e != nil {
			return e
		}
		migrations = append(migrations, migration)

		return nil
	})
	if e != nil {
		return nil, e
	}

	return migrations, nil
}

func createMigration(path string, info os.FileInfo) (Migration, error) {
	file, e := os.Open(path)
	if e != nil {
		return nil, e
	}
	defer file.Close()

	downData, e := ioutil.ReadFile(path)
	if e != nil {
		return nil, e
	}
	upData, e := ioutil.ReadFile(strings.ReplaceAll(path, ".down.", ".up."))
	if e != nil {
		return nil, e
	}

	version, e := strconv.ParseUint(strings.Split(info.Name(), "_")[0], 10, 0)
	if e != nil {
		return nil, e
	}

	return &migration{
		version: version,
		up:      string(upData),
		down:    string(downData),
	}, nil
}
